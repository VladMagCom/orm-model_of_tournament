from tortoise import fields
from tortoise.models import Model
from tortoise import Tortoise, run_async
import random
from typing import List, Generator, Iterator
import more_itertools as mit
from dataclasses import dataclass
import asyncio


class Tournament(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()

    def __str__(self):
        return self.name

class Place(Model):
    id = fields.IntField(pk = True)
    name = fields.TextField()

    def __str__(self):
        return self.name

class Event(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()
    place = fields.ForeignKeyField('models.Place', related_name = 'events')
    tournament = fields.ForeignKeyField('models.Tournament', related_name='events')
    participants = fields.ManyToManyField('models.Team', related_name='events', through = 'event_team')

    def __str__(self):
        return self.name


class Team(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()
    members = fields.ManyToManyField('models.Player', related_name = 'teams', through = 'player_team')

    def __str__(self):
        return self.name

class Player(Model):
    id = fields.IntField(pk = True)
    name = fields.TextField()

    def __str__(self):
        return self.name

def random_name() -> str:
    names = ["Bob", "Leo", "Kan", "Timur", "Sacha", "Olia", "Lera"]
    surnames = ["Popov", "Smirnow", "Freeman", "Johnson", "Jackson", "Brown"]

    name = random.choice(names)
    surname = random.choice(surnames)
    return f"{name} {surname}"

def random_players() -> List[str]:
    size = random.randint(3, 5)
    return [random_name() for _ in range(size)] 

def random_place() -> str:
    places = ["Moscow", "SainPi", "Rome", "London", "Omsk", "Barcelona"]
    return random.choice(places)

async def create_tournament_event_team():

    tournament = Tournament(name="Tournament 1")
    await tournament.save()

    place = Place(name="Moscow")
    await place.save()
    
    event = Event(name="Event 1", tournament = tournament, place = place)
    await event.save()
    
    team = Team(name="Team 1")
    await team.save()
    
    await event.participants.add(team)

    player = Player(name="Messi")
    await player.save()
    
    await team.members.add(player)

@dataclass
class TourResult:
    loser:str 
    winner:str

def playgame(team_1: str, team_2: str) -> TourResult:
    if random.random() > 0.5:
        return TourResult(loser=team_1, winner=team_2)       
    return TourResult(loser=team_2, winner=team_1)
    


def simulate_tournament(teams: List[str]) -> Iterator[TourResult]:
    if len(teams) < 2:
        return 
    winners = []
    for pair in mit.chunked(teams, 2):
        if len(pair) == 1:
            winners.insert(0, pair[0])
            continue            
        tourresult = playgame(pair[0], pair[1])
        yield tourresult
        winners.append(tourresult.winner)
    yield from simulate_tournament(winners)

    
    

async def create_BD():
    all_teams = ["Snakes", "Bears", "Bulls", "Clops", "Lions", "Flyes", "Wolfs"]

    tournaments = ["Big Ball", "Star", "Crown"] 

    teams_split = mit.windowed(all_teams, 5)
    

    for tournament, teams in zip(tournaments, teams_split):
        
        tournament_db = await Tournament.create(name = tournament)
        team_mapping = {}
        for team in teams: #TODO: create asyncreation team 
            #members_db = [await Player.create(name = player) for player in random_players()]
            

            members_db = await asyncio.gather(
                *[Player.create(name = player) for player in random_players()]
            )
            team_db = await Team.create(name = team)
            await team_db.members.add(*members_db)
            team_mapping[team] = team_db

        tours = simulate_tournament(teams)
        for tourresult in tours:
            place_db, _ = await Place.get_or_create(name = random_place())
            event_name = f"{tourresult.winner} vs {tourresult.loser} "
            event_db = await Event.create(
                name = event_name,
                place = place_db,
                tournament = tournament_db,
                
            )
            await event_db.participants.add(team_mapping[tourresult.winner])
            await event_db.participants.add(team_mapping[tourresult.loser])
            
        




    

    

async def main():
    
    
    db_url = 'sqlite://db.sqlite'
    

    await Tortoise.init(db_url = db_url, modules={'models': ['__main__']})
    await Tortoise.generate_schemas()

    await create_BD()

    
    

if __name__ == "__main__":
    run_async(main())
